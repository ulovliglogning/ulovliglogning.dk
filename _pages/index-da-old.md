---
layout: indexpage
lang: da
permalink: /old
sections:
  - id: intro
    heading: |
      EU: Højesteret tog fejl
    content: |
      Efter fem års hårdt, ulønnet arbejde kom dagen: Højesteret afsagde dom over logningen. Det var ubestridt at den var ulovlig, men Højesteret mente ikke at det var deres opgave at stoppe den. Det har [en EU-dom](https://curia.europa.eu/juris/document/document.jsf?text=&docid=257242&pageIndex=0&doclang=EN&mode=req&dir=&occ=first&part=1&cid=325817) korrigeret, kun seks dage senere.
      
      Danmarks højeste domstol var enig med Østre Landsret i at menneskerettigheder kun kan håndhæves af teleselskaber. Hvis de undlod at logge, og fik en bøde for det, ville de kunne gå til domstolene. Borgerne har ingen muligheder. Det er klart i strid med Den Europæiske Menneskerettighedskonvention og EU's Charter om Grundlæggende Rettigheder, men det undlod Højesteret at inddrage.
      
      EU-Domstolen gjorde det efterfølgende meget klart at de nationale domstole **skal** dømme sådan regulering ugyldig. Vi arbejder pt. på der videre forløb.
      
      Læs højesteretsdommen [her (pdf)](https://ulovliglogning.dk/assets/docs/hrdom.pdf) og [EU-dommen her](https://curia.europa.eu/juris/document/document.jsf?text=&docid=257242&pageIndex=0&doclang=EN&mode=req&dir=&occ=first&part=1&cid=325817). Se især afsn. 123.

      For mere information, kontakt talsperson Rasmus Malver på 26 80 94 24.
  - id: butwhy
    link: Hvorfor?
    heading: Hvorfor er det vigtigt?
    content: |
      I en demokratisk retsstat har borgerne tillid til staten, og alle har ret til en retfærdig rettergang. Logningsdata indsamles ulovligt, men kan alligevel bruges i en straffesag imod dig. Dansk politi kan rejse tiltale og varetægtsfængsle udelukkende på baggrund af hvor de siger at din telefon har været.
      
      Måske er du gået forbi et sted hvor der blev begået en forbrydelse. Politiet får en liste over alle telefoner i området, og så skal du bevise at du ikke var skyldig.
      
      Men du kan ikke bruge det i dit forsvar. Hvis du beviser at din telefon ikke var på gerningsstedet, kan anklageren bruge det som bevis for at du havde planlagt din forbrydelse. Politiet har lettere og hurtigere adgang til dine data, og der er ingen garanti for at de deler data med din forsvarsadvokat.

       I teleskandalerne lærte vi at visse teleselskaber ulovligt logger dine sms'ers indhold, og ulovligt deler dem med politiet. Politiet indrømmede også at de i årevis har ændret data, uden at sige det til forsvarsadvokater og dommere.

      FN's verdensmål nr. 16 handler om at støtte fredelige og inkluderende samfund. Alle skal have adgang til retssikkerhed og samfund skal have effektive, ansvarlige og inddragende institutioner på alle niveauer. Vores retssag er et vigtigt skridt i retning af at opnå dette mål. Vi skaber retspraksis der giver [EU's menneskerettigheder](https://eur-lex.europa.eu/legal-content/DA/TXT/HTML/?uri=CELEX:12012P/TXT) direkte virkning i Danmark. Tidligere kunne et simpelt flertal i Folketinget tilsidesætte de fleste rettigheder.

      Det standser vi.
  - id: tellmemore
    link: Hvordan?
    heading: Jeg vil vide mere…
    content: |
      Teleindustrien og de største teleselskaber kæmper for at logge, og bruger et [personligt brev](nicksbrev) fra justitsminister Nick Hækkerup som undskyldning. I brevet trygler han organisationens medlemmer om at fortsætte logning. Det er et tomt ønske, for det har ingen hjemmel. Logningsdirektivet som lå til grund for den danske lognings&shy;bekendt&shy;gørelse er blevet underkendt ved EU-domstolen, fordi det krænker retten til privatliv. Menneskeretten er inkorporeret i både dansk ret og EU-retten, og en minister kan derfor ikke udstede bekendtgørelser der krænker borgernes rettigheder.

      Sagen kunne teoretisk anlægges imod et teleselskab, men med brevet fra ministeren kunne de påstå uvidenhed. I samråd med vores advokat er målet at få den danske stat dømt for at overtræde dansk lov, EU- og menneskeretten. Det er en større og dyrere sag end at sagsøge et teleselskab, men den vil til gengæld standse politikeres tendens til at ignorere vores fundamentale rettigheder.

      Hvis du vil læse processkrifterne, vores vedtægter, vores regnskaber eller andre relevante dokumenter kan du finde dem her:

      * [Vedtægter](/assets/docs/vedtaegter.pdf)
      * [Regnskab 2018-2019](/assets/docs/Regnskab2018.pdf)
      * [Regnskab 2019-2020](/assets/docs/Regnskab2019.pdf)
      * [Regnskab 2020-2021](/assets/docs/Regnskab2020.pdf)
      * [Regnskab 2021-2022](/assets/docs/Regnskab2021.pdf)
      * [Regnskab 2022-2023](/assets/docs/Regnskab2022.pdf)
      * [Indsamlingstilladelse](/indsamlingstilladelse)

      * [Stævning](/assets/docs/2018-06-01-staevning.pdf)
      * [Svarskrift](/assets/docs/2018-09-24-svarskrift.pdf)
      * [Replik](/assets/docs/2021-01-07-replik.pdf)
      * [Duplik](/assets/docs/2021-02-25-duplik.pdf)
      * [Processkrift 1 \(os\)](/assets/docs/2021-03-25-processkrift1.pdf)
      * [Processkrift 2 \(Kammeradvokaten\)](/assets/docs/2021-04-21-processkrift2.pdf)

      * [Ekstrakt (alle ovenfor, 56,1 MB)](/assets/docs/2021-04-28-ekstrakt.pdf)
      * [Materialesamling (øvrige dokumenter, 65 MB)](/assets/docs/2021-05-06-materialesamling.pdf)
      * [Højesteretsdom](/assets/docs/2022-03-30-hrdom.pdf)

  - id: wannahelp
    link: Hjælp/bidrag!
    heading: Jeg vil hjælpe!
    content: |
      Hvis du har lyst til at hjælpe, kan du kontakte [Rasmus Malver](https://twitter.com/rasmusmalver) på [Twitter](https://twitter.com/rasmusmalver) eller på [sms/signal](sms:+4526809424). Du kan også følge foreningen på [Twitter](https://twitter.com/ulovliglogning)/[Facebook](https://www.facebook.com/UlovligLogning/). Eller skrive dig op til vores nyhedsbrev ved at sende en mail til [info@ulovliglogning.dk](mailto:info@ulovliglogning.dk).

      Du kan også hjælpe ved at skabe opmærksomhed. Kontakt dit netværk, journalister og din familie, og fortæl hvorfor det er vigtigt at kæmpe for vores basale rettigheder.
  - id: faq
    link: FAQ
    content:
    - heading: Hvad med Søren Pape?
      id: hvorforPape
      answer: Han er ikke længere justitsminister.
      explanation: |
        Logning har altid været ulovligt. Søren Pind, Mette Frederiksen, Karen Hækkerup, Morten Bødskov, Brian Mikkelsen, Søren Pape og Nick Hækkerup har logget ulovligt som justitsministre, så det er ikke et spørgsmål om politiske holdninger. Sagen fortsætter ufortrødent, nu hvor Peter Hummelgaard er minister. Det er et spørgsmål om at respektere fundamentale rettigheder, herunder alles ret til privatliv.

        Pape skilte sig dog ud fra mængden, fordi han skrev et [brev til telebranchen](papesbrev). Det var ulovligt at han skrev brevet, men kun Folketinget kunne have stillet ham til ansvar for brud på ministeransvarsloven. Måske inspirerede han Nick Hækkerup, som i 2020 skrev [et tilsvarende brev](nicksbrev).

        Sagen vil forhåbentlig ændre danske politikeres åbenlyse og intentionelle overtrædelser af menneskeretten. Men uanset om den gør, ændrer den retspraksis.
    - heading: Hvorfor ikke TDC?
      id: hvorforIkkeTDC
      answer: Fordi Pape skrev et brev, der bad dem overvåge ulovligt.
      explanation: |
        TDC’s påstand var *juridisk vildfarelse*. Det betyder at de ikke kan stilles til ansvar for at gøre noget ulovligt, fordi de ikke kunne forventes at forstå at det var ulovligt. Med [Papes brev til Teleindustrien](papesbrev) står de bedre. Men der er stadig en forventning om at man skal kunne indse at en ordre er ulovlig. Også når den kommer fra en minister.

        Det er både hårdere og dyrere at gå efter Justitsministeriet i stedet, men til gengæld kan det ændre retstilstanden i Danmark. Og måske politikernes aktive overtrædelse af vores rettigheder.

        Med dommen fra Menneskerettighedsdomstolen er det oplagt at tage en GDPR-sag mod TDC og de andre netværk.
    - heading: Er der domstolskontrol med adgang til data?
      id: domstolskontrol
      answer: Nej.
      explanation: |
        Hvis du traditionelt skulle aflyttes, skulle politiet gå til domstolene, og her blev du tildelt en advokat. Når aflytningen var færdig, fik du det at vide. Når nogen tilgår dine logningsdata får du intet at vide.

        Hvis politiet vil have logningsdata om dig, er det op til telebranchen at hyre og lønne en advokat på dine vegne. Det gør de (selvfølgelig) ikke, og derfor kan domstolene ikke træffe en rimelig afgørelse. Når det drejer sig om oplysninger om IP-adresser har politiet forfattet en standard-skrivelse, der tager udgangspunkt i at udbyderne altid udleverer hvad der bedes om. I to svar til Folketinget oplyser Justitsministeriet at man ikke altid indhenter dommerkendelse. Se Retsudvalgets [spørgsmål 624](https://www.ft.dk/samling/20171/almdel/reu/spm/624/svar/1498873/1913158/index.htm) og [627](https://www.ft.dk/samling/20171/almdel/reu/spm/627/svar/1498872/1913155/index.htm) (alm. del 2017/18). I 627 indrømmes det at der ikke altid indhentes retskendelse.

        Når teleselskaberne selv tilgår dine data, bliver ingen hørt eller orienteret.
    - heading: Hvorfor skal jeg bekymre mig om logning? Jeg har intet at skjule, så hvis det hjælper mod kriminalitet går jeg ind for logning!
      id: whyBother
      answer: Det er en myte at logning hjælper politiet.
      explanation: |
        Total overvågning hjælper ikke nødvendigvis imod kriminalitet. Det **kan** give flere sigtelser og dømte, men primært fordi flere uskyldige vil blive straffet. Hvis der bliver begået en forbrydelse i en demokratisk retsstat skal politiet og ofrene arbejde sammen for at identificere hvem der kunne have en interesse i at begå forbrydelsen, hvem der havde skaffet sig adgang til gerningsstedet, og hvem der har udvist mistænkelig adfærd.

        Med totalovervågning kan politiet trække en liste over alle personer der var i nærheden af gerningsstedet, og derefter vælge hvem de lettest kan få dømt. Det er derfor dit ansvar at bevise at du ikke har begået en forbrydelse. Anklageren behøver ikke finde et motiv, eller bevise at du har handlet på en bestemt måde. De kan bare vælge dig fra listen over mobiltelefoner der har været i området, eller blandt folk der har googlet “brækjern” 24 timer før.

        Når logning standser bliver politiet ikke forvandlet til muldvarpe. Der er stadig vidtgående muligheder for at overvåge folk på grund af konkret mistanke, men politiet skal igen kunne argumentere for indgrebet.
        
        I Tyskland blev logningen standset tidligt. Det tyske Max Planck-institut for udenlandsk og international strafferet har lavet [en kvalitativ undersøgelse](https://static.mpicc.de/shared/data/pdf/schutzluecken_vorratsdatenspeicherung_12.pdf) som viser at logningsdata ikke hjalp politiet med opklaring.
    - heading: Hvem står bag søgsmålet?
      id: whoAreYou
      answer: Foreningen imod Ulovlig Logning.
      explanation: |
        Menneskeretsjurist [Rasmus Malver](https://twitter.com/rasmusmalver) startede indsamlingen, og den næste store donor var [Bitbureauet](https://bitbureauet.dk/). Derfra tog det fart, og flere hundrede andre personer, virksomheder og foreninger har doneret til sagen. De indsamlede penge “tilhører” en forening hvis eneste formål er at føre retssagen og at sprede budskabet. Du kan læse [vedtægterne](vedtaegter) her.

        Foreningen har valgt IT- og EU-retsspecialistkontoret [Bird & Bird](https://www.twobirds.com), hvor advokat Julie Bak-Larsen er primær tovholder.

        Du kan finde vores reviderede regnskab under [jeg vil vide mere](#tellmemore).

        I bestyrelsen for Foreningen imod Ulovlig Logning sidder Rasmus Malver, Henrik Kramselund Jereminsen, Mikkel Mikjær, Thomas Bødtcher Hansen og Alex Holst.
    - heading: Hvad har logning med menneskeret at gøre?
      id: humanRights
      answer: Du har ret til privatliv.
      explanation: |
        Menneskeret er dine rettigheder overfor stater. I nogle lande fremgår de af forfatningen, men i Danmark er de primært kommet fra Den Europæiske Menneskerettighedskonvention ([pdf](https://www.echr.coe.int/Documents/Convention_DAN.pdf)). Den blev skrevet efter 2. verdenskrig og er løbende blevet opdateret, for at undgå en gentagelse af Nazityskland og Østblokkens rædsler. I år 2000 skrev EU et Charter om Grundlæggende Rettigheder ([dansk](https://eur-lex.europa.eu/legal-content/DA/TXT/?uri=CELEX%3A12012P%2FTXT)) der indgår på overstatsligt niveau, dog kun for emner omfattet af EU-samarbejdet.

        Begge konventioner indeholder en beskyttelse af dit privatliv, og det er slået klart fast at staten ikke må overvåge alle konstant. Men det gør Danmark. 

        På grund af logningen ved staten altid hvor din mobil er, om du er på nettet, og hvem du kommunikerer med. Der indsamles mere information om dig og din adfærd end STASI og Gestapo nogensinde kom i nærheden af.
        Du har ret til at være fri for dén overvågning.
    - heading: Hvorfor ikke fri proces?
      id: hvorforIkkeFriProces
      answer: Man får ikke penge nok, så sagen ville dø, før den startede.
      explanation: |
        Hvis staten tildeler fri proces, må man ikke selv betale en del af advokatens regning. Fri proces dækker dog ikke advokatens egentlige regning, men består i stedet af et symbolsk beløb. Den danske stats advokat, Kammeradvokaten, er én af verdens dyreste advokater, men deres regning indgår ikke i udmålingen af det de tilkendte sagsomkostninger ved fri proces.

        Derfor er en fri proces-sag mod Kammeradvokaten en meget ulige kamp. Staten skulle dække begge siders omkostninger, men Kammeradvokaten kan selv bestemme hvor meget de skal have, mens staten bestemmer hvad der betales til borgerens advokat. Det er naturligvis i strid med retten til en retfærdig rettergang, men de danske domstole lader til at foretrække status quo.
        
        Du kan læse mere om emnet i bogen <a href="https://www.ft.dk/samling/20121/almdel/reu/bilag/68/1176705.pdf">“Med Staten som Modpart” (pdf)</a> udgivet af Retssikkerhedsfonden.
    - heading: Økonomi & Delmål
      id: delmaal
      answer: Vi ved ikke hvad det vil koste, så vi bliver ved med at samle ind.
      explanation: |
        Da vi nåede vores første delmål, 100.000 kr, skrev vores advokater [en udførlig stævning](staevning). Med pengene fra det næste delmål, 250.000 kr, havde vi råd til at stævne justitsminister Søren Pape, som repræsentant for den danske stat.

        Vi ved ikke hvor meget det vil ende med at koste. Det er dyrt at sikre vores rettigheder, og vi er gået hele vejen!

    - heading: Tjener I nogle penge på det her? Hvad hvis der er penge i overskud?
      id: areYouMakingMoney
      answer: Nej, og hvis det er penge til overs går de til et lignende formål.
      explanation: |
        Pengene går til at betale advokatkontoret ([Bird & Bird](https://www.twobirds.com)) og til at betale sagsomkostninger. Hvis der er penge “til overs” vil de gå til informationsmateriale om logning og/eller til en non-profit-organisation med samme formål.
    - heading: Hvad er det egentligt der bliver logget om mig?
      id: whatIsBeingLoggedAboutMe
      answer: Bl.a. hvilken telefon du har, hvor du er og hvem du kommunikerer med.
      explanation: |
        Hvor din mobil er, og hvem du kommunikerer med. Du kan bede din udbyder sende dig en kopi af alt hvad de har registreret. I teleskandalerne viste det sig at de også logger indhold, og upfordret deler det med politiet.
    - heading: Ministeren siger at han skal bruge tid på at ændre lovgivningen, det er vel fair nok?
      id: ministerFair
      answer: Nej.
      explanation: |
        Allerede da totalovervågningen blev indført fik den danske stat at vide at det ville være ulovligt.

        En lov og en bekendtgørelse kan være ulovlige, hvis de eks. strider imod en overstatslig regel, i dette tilfælde [EU-Charteret](https://eur-lex.europa.eu/eli/treaty/char_2012/oj?locale=da), eller hvis de underforstået ønsker at overholde Menneskerettighedskonventionen.

        Ved Digital Rights-dommen blev det slået fast at totalovervågning var i strid med Charteret, og i [Tele2/Watson-dommen](https://eur-lex.europa.eu/legal-content/DA/TXT/?uri=CELEX%3A62015CJ0203) blev det slået fast igen. Intet af dette har været en overraskelse, og dommene betyder “kun” at staten ikke kan bruge uvidenhed som undskyldning.

        Justitsministeriet ønsker at indføre en ny form for overvågning. Det er deres ret at foreslå ny lovgivning, men det betyder ikke at man kan opretholde en igangværende forbrydelse. 

        Hvis man går over for rødt må man ikke standse midt i krydset, og blive stående indtil man har opfundet en jetpack så man i fremtiden kan flyve over for rødt. Hvis man laver et bankrøveri og alarmen går, er det ikke en lovlig undskyldning at tage gidsler, mens man prøver at finde på en plan for sit næste bankrøveri.

        Logning er en kriminel handling og den eneste grund til at justitsministeren ikke sidder i fængsel, er at kun Folketinget kan stille ham foran en dommer.
    - heading: Har I overvejet borgerforslag.dk?
      id: borgerforslag
      answer: Ja, men det vil smadre hele formålet.
      explanation: |
        Vi har fravalgt borgerforslag.dk, fordi det ikke er et politisk spørgsmål. Logningen er ulovlig, og det er der ingen tvivl om. Retssagen bliver en kavalkade af undvigelsesmanøvrer, forsinkelsestaktikker og bullshitting fra statens side. Kammeradvokaten er specialist i at ændre fokus, og det bliver en cirkusforestilling af en anden verden.
        
        Derfor er det nødvendigt at vi holder fokus. Et borgerforslag kan blive til et beslutningsforslag i Folketinget, og det vil fjerne fokus. Det vil også være i strid med [Grundlovens § 3](https://www.retsinformation.dk/Forms/R0710.aspx?id=45902#P3), for Folketinget må kun bestemme politiske ting. De må ikke sige at noget er ulovligt. De kan forholde sig til at noget er ulovligt, men det gør de allerede. Langsomt.

        Folketinget kan også stille Nick Hækkerup, Mattias Tesfaye og Peter Hummelgaard for en dommer, iht. [Grundlovens § 13](https://www.retsinformation.dk/Forms/R0710.aspx?id=45902#P13). Men kun i Rigsretten, iht. [Grundlovens § 16](https://www.retsinformation.dk/Forms/R0710.aspx?id=45902#P16). Og det skal ske i [Udvalget for Forretningsordenen](https://www.retsinformation.dk/Forms/R0710.aspx?id=186146#idf4f8c639-b3b8-4850-a0e5-18273ab31d25), hvor alle partier er repræsenteret. Men ingen har foreslået det. En folketingsbeslutning vil hverken gøre til eller fra. Den vil bare blive makuleret, fordi det ikke er sådan man stiller ministre til ansvar.

        Vi har kontakt til alle Folketingets partier, og vi gør hvad vi kan for at holde offentlighedens fokus. Underskriftindsamlinger og borgerforslag er en velment kæp i hjulet. Hvis du har lyst til at hjælpe os, kan du skrive til folketingets partier, medierne, dine venner og din familie. Hver dag bliver der opsamlet data på ulovlig vis, og det sætter uskyldige bag tremmer [eller værre](https://www.youtube.com/watch?v=PxwEwwlDM8Q).
    - heading: Må I samle penge ind?
      id: indsamlingstilladelse
      answer: Ja
      explanation: |
        Vi er [godkendt](/indsamlingstilladelse) af Indsamlingsnævnet.
---