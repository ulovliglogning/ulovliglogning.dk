---
layout: indexpage
lang: en
permalink: /
sections:
  - id: intro
    heading: It's all over
    content: We lost. Politicians won. The European Convention on Human Rights is no longer applicable in Denmark, and the national courts have stopped whatever limited application they ever did.
---