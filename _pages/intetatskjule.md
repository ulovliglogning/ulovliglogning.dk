---
layout: singlepage
lang: en
permalink: /intetatskjule
id: intetatskjule
heading:
---

# Perfekt til julehyggen!

Når man [giver en donation i julegave](https://ulovliglogning.dk/gavekort), eller opfordrer venner og familie til at donere, kan man risikere at møde påstanden *“jeg har ikke noget at skjule”*. Vi har det perfekte redskab til de situationer: En fuldmagt. 

Med fuldmagten indhenter Foreningen imod Ulovlig Logning, på deres vegne, dokumenter og informationer fra alle de kendte kilder. Inklusiv, selvfølgelig, logningsdata. Med de indhentede data skaber vi en profil af personen, som vi sender tilbage til vedkommende. Efter profilen er afsendt sletter vi alle data, inkl. fuldmagten, og gør alle tredjeparter opmærksomme på at vi ikke længere repræsenterer fuldmagtsgiver.

For mere information, kontakt gdpr \[hos\] ulovliglogning.dk.

Postkortene er gratis, men du må [meget gerne donere](https://ulovliglogning.dk#wannahelp). De kan bl.a. hentes i [Labitat](https://labitat.dk) på HC Ørstedsvej 5 på Frederiksberg. Vi kan sende dem, men du skal selvfølgelig selv betale portoen. Kontakt os på info \[hos\] ulovliglogning.dk.