---
layout: indexpage
lang: da
permalink: /
sections:
  - id: intro
    heading: |
      Lukket
    content: |
      Efter Højesteret og Menneskerettighedsdomstolens afgørelser, har vi været nødt til at lukke foreningen.
      
      Det fejrede [Caroline Stage Olsen og regeringen](https://www.digmin.dk/digitalisering/nyheder/nyhedsarkiv/2024/nov/regeringen-og-telebranchen-daemmer-op-for-falske-opkald-og-sms-beskeder) ved at bede Teleindustrien læse alle dine sms'er fremover.
  - id: tellmemore
    link: Dokumenter
    heading: Dokumenter og regnskaber
    content: |
      * [Vedtægter](/assets/docs/vedtaegter.pdf)
      * [Regnskab 2018-2019](/assets/docs/Regnskab2018.pdf)
      * [Regnskab 2019-2020](/assets/docs/Regnskab2019.pdf)
      * [Regnskab 2020-2021](/assets/docs/Regnskab2020.pdf)
      * [Regnskab 2021-2022](/assets/docs/Regnskab2021.pdf)
      * [Regnskab 2022-2023](/assets/docs/Regnskab2022.pdf)
      * [Regnskab 2023-2024](/assets/docs/Regnskab2023.pdf)

      * [Stævning](/assets/docs/2018-06-01-staevning.pdf)
      * [Svarskrift](/assets/docs/2018-09-24-svarskrift.pdf)
      * [Replik](/assets/docs/2021-01-07-replik.pdf)
      * [Duplik](/assets/docs/2021-02-25-duplik.pdf)
      * [Processkrift 1 \(os\)](/assets/docs/2021-03-25-processkrift1.pdf)
      * [Processkrift 2 \(Kammeradvokaten\)](/assets/docs/2021-04-21-processkrift2.pdf)

      * [Ekstrakt (alle ovenfor, 56,1 MB)](/assets/docs/2021-04-28-ekstrakt.pdf)
      * [Materialesamling (øvrige dokumenter, 65 MB)](/assets/docs/2021-05-06-materialesamling.pdf)
      * [Højesteretsdom](/assets/docs/2022-03-30-hrdom.pdf)

      * [Menneskerettighedsdomstolens dom](/assets/docs/2024-09-05-EMD-dom.pdf)

  - id: wannahelp
    link: Hjælp/bidrag!
    heading: Jeg vil hjælpe!
    content: |
      Det er for sent. Menneskerettigheder i Danmark er et lukket kapitel.
---