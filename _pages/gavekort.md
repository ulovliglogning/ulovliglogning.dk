---
layout: singlepage
lang: da
permalink: /gavekort
id: gavekort
heading:
---

# Giv en donation!

I stedet for at give fysiske ting, som modtager alligevel mister interessen for i februar, kan du give sagen en donation i deres navn. Du får et pænt kort hvor vi siger tak, og hvor du kan skrive din egen hilsen. Hvis du vil have det, kan vi skrive det under.

Du skal donere mindst 200 kr., plus 25 kr. til for forsendelse. Send derefter en besked til gavekort \[ hos \] ulovliglogning.dk med navn og adressse, og om du vil have at vi skriver hilsenen under. Du kan give på Mobile Pay 40456 eller med kontooverførsel til 5301 272500. Du kan læse mere om [hvordan du kan donere her](https://ulovliglogning.dk/#wannahelp).

Har du familiemedlemmer eller venner som *“ikke har noget at skjule”*, har [vi også et kort til dét](https://ulovliglogning.dk/intetatskjule). Du får automatisk ét med, hvis du bestiller donations-kortet.