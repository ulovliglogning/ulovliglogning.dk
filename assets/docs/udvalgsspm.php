<!doctype html>

<html lang="da">
<head>
  <meta charset="utf-8">
  <title>Spørgsmål til justitsministeren</title>
  <meta name="description" content="">
  <meta name="author" content="Foreningen imod ulovlig logning">
  <style type="text/css">
  	html,body{
  		font:normal 14px 'Droid Sans', sans-serif;
  		background:#444;
  		color:#FFF;
  	}
  	li{margin:0 0 1em 0;line-height:1.35;}
  	@media screen and (orientation:landscape) {
	  	li{max-width:600px;}
		}
  </style>
</head>
<body>
<h1>Udvalgsspørgsmål, REU alm. del</h1>
<h2>Vigtigst</h2>
<ul>
<li>Ministeren bedes oversende samtlige notater og skriftlig kommunikation fra Niels Dam Dengsøe Petersen til sin overordnede og ministeren der vedrører logning, herunder især udarbejdelse af ministerens brev til Teleindustrien  af 16. marts 2017.</li>
<li>Ministeren bedes oplyse om nogen ansat har gjort indsigelse imod legaliteten af ministerens adfærd, ministerens brev til telebranchen, ministerens fortolkning og ministerens besvarelse af Folketingets spørgsmål.</li></ul>
<h2>I øvrigt</h2><ul>
<li>Ministeren oplyser i brev af 11. januar 2018 at “Danmark såvel som de øvrige berørte medlemslande opretholder deres gældende logningsregler indtil videre”. Kan ministeren bekræfte at der logges på niveau med Danmark i Tyskland, Nederlandene og Østrig?</li>
<li>I skriftlig kommentar til Ritzaus bureau ifm. ovenfor nævnte brev oplyser ministeren at “databeskyttelsen [skal] være i orden, ligesom telebranchen ikke skal pålægges unødige byrder”. Ministeren bedes forklare hvordan ophævelse af den menneskeretstridige logning kræver yderligere databeskyttelse og hvordan den pålægger telebranchen yderligere byrder.</li>
<li>Ministeren bedes redegøre for rationalet bag at en bekendtgørelse kan opretholdes når dens hjemmel kendes ugyldig.</li>
<li>Ministeren bedes redegøre for at den opretholdte totalovervågning er i overensstemmelse med EU-charteret for fundamentale rettigeder.</li>
<li>Ministeren bedes redegøre for at den opretholdte totalovervågning er i overensstemmelse med den Europæiske Menneskerettighedskonvention.</li>
<li>Ministeren bedes redegøre for det forfatnings- og forvaltningsretlige  grundlag for at opretholde en bekendtgørelse der strider imod indkorporeringsloven af 29. april 1992, som bekendtgjort i LBK 750 af 19/10 1998.</li></ul>
</body>
</html>
