#!/bin/bash

error="\033[0;31m"
success="\033[0;32m"
normal="\033[0m"

#get config vars
source _scripts/config.sh

if [[ -z $jekyll_dir || -z $remote_username || -z $remote_password || -z $remote_host || -z $remote_dir ]]; then
	echo -e "${error}FAILED: You have ommited one or more values from the config file, see the 'config-example.sh' file for the required values. Or maybe you need to create a 'config.sh' file in the '_scripts' directory.${normal}"
	exit 1
fi

echo -e "${success}Starting build…${normal}"

#Disable SSL check for lftp, this fails on our host :(
echo "set ssl:verify-certificate no" > ~/.lftprc

if [ -d "$jekyll_dir" ]; then
	jekyll build --strict_front_matter
	if [[ $? != 0 ]]; then
		echo -e "${error}FAILED: DID NOT DEPLOY!${normal}"
		exit 1
	else
		echo -e "${success}Build succeded, now uploading to FTP server${normal}"
		lftp -u ${remote_username},${remote_password} -e "mirror -R -p -P=10 --only-newer -v ${jekyll_dir}/_site/ ${remote_dir};quit" ftp://${remote_host}
		if [[ $? != 0 ]]; then
			echo -e "${error}FAILED: Something went wrong trying to upload to the FTP server${normal}"
			exit 1
		else
			echo -e "${success}FTP upload complete, deployment DONE!${normal}"
		fi
	fi
fi