# Ulovlig Logning
## Hvad er logning?
Da logningen blev indført, omfattede den overvågning af din trafik på internettet, og meget af hvad du hvad laver på din mobiltelefon. Konkret blev tid, sted og samtalepartnere registreret når du sendte beskeder, ringede eller brugte internettet fra mobilen. Internetovervågningen blev sat på pause da de første domme kom, og i øjeblikket bliver der "kun" logget oplysninger om opkald og beskeder. Afhængigt af din brug af telefonen kan man lave et relativt detaljeret kort over din færden.

## Hvem bliver logget?
Alle med en mobiltelefon. Det behøver ikke at være en smartphone. Du kan se hvad der bliver gemt om dig, hvis du beder dit mobilselskab om at udlevere en fuld indsigt.

## Hvem logger?
Det gør de danske teleselskaber. Der er tre mobilnetværk i Danmark, TDC, 3 og Telenor/Telia, og det er primært de selskaber der foretager overvågningen. I andre lande, bl.a. Sverige, har teleselskaberne været på borgernes side, og de har nægtet at foretage den ulovlige logning.

I Danmark henviser teleselskaberne til et brev fra justitsminister Søren Pape Poulsen, hvor han kræver at de fortsætter logningen, selvom den er kendt ulovlig.

[Se _“Hvad gør Folketinget?”_ og _“Hvilke rettigheder har vi?”_]{custom-style="ref"}

## Hvorfor logges der?
Ifølge lognings-forkæmpere er logning løsningen på kriminalitet, terror og alt ondt i verden. Der er ingen data der understøtter disse påstande. Tværtimod understreger politiets egen evaluering af Krudttøndeangrebet at logning ikke kunne bruges til noget, da gerningspersonen havde mere end én telefon. Det illustrerer fint hvorfor generel, udifferentieret logning bare ikke virker: Folk der planlægger forbrydelser kan let omgå det, mens lovlydige borgere ikke orker besværet.

[Se _“Hvad bliver logningsdata brugt til?”_]{custom-style="ref"}

## Hvad bliver logningsdata brugt til?
Staten bruger logningsdata til at straffe folk. Det er relevant at data kun hjælper anklageren. Logning kan bevise at en tiltalt var på et bestemt sted på et bestemt tidspunkt, men hvis data siger det modsatte kan forsvareren ikke bruge det. For så beviser data jo bare at tiltalte havde planlagt forbrydelsen.

Teleselskaberne har brugt data til at bl.a. at overvåge bevægelsesmønstre og hvor længe folk opholder sig på bestemte steder.

Overvågning har desuden en indirekte følgevirkning: Når man bliver observeret af andre, ændrer man sin adfærd. Totalitære regimer forsøger ofte at skabe en konstant usikkerhed hos borgerne, så ingen tør gøre oprør.

[Se _Hvorfor logges der?_ og _Det er uundværligt for politiet_]{custom-style="ref"}

## Hvorfor er I så sure?
Der er mange gode grunde til at ulovlig overvågning er et problem. I de øvrige svar forklarer vi hvorfor overvågning er et problem, men der er også andre udfordringer. Den første er at staten tilsidesætter borgernes rettigheder på ulovlig vis. Strafniveauet for borgeres forbrydelser hæves i næsten alle folketingssamlinger, men når politikere og staten bryder loven er der ingen konsekvens.

I en demokratisk retsstat er man _lige for loven_, og det forudsættes at især magthaver kigges efter i sømmene. Det er ikke tilfældet i Danmark, men i denne sag har vi en unik mulighed: EU's menneskerettigheder er hævet over Folketinget. Det betyder at politikerne ikke kan vælge at tilsidesætte dem. Ved hjælp af domstolene kan vi som borgere standse en uhyggelig tendens. Derfor er det en vigtig sag.

[Se _Hvorfor er det ulovligt?_ og _Hvilke rettigheder har vi?_]{custom-style="ref"}

## Hvad gør de andre EU-lande?
Mange lande har stoppet den ulovlige overvågning. Nogle lande, eks. Tyskland og Nederlandene, har stoppet det af egen drift, fordi det så åbenlyst er en krænkelse af menneskeretten. Andre lande, som Irland, UK og Sverige, har stoppet det på baggrund af dommene fra EU.

## Hvad skal vi gøre i stedet?
Konkret, specifik overvågning. Der må kun overvåges, hvis der er en konkret, forudgående mistanke om et kriminelt forhold.

## Hvad betyder "generel, udifferentieret logning"?
At man overvåger alle mennesker på alle tidspunkter. Det modsatte er konkret, specifik logning.

Statens formål er at beskytte og at tjene borgerne. Det kan være nødvendigt at tilsidesætte én borgers rettigheder for at beskytte en anden borger, men menneskerettighederne stiller en række krav til indgrebet. Først og fremmest et princip om rimelighed og proportionalitet: Hvad forsøger staten at opnå ved at tilsidesætte borgernes rettigheder? Kunne det samme opnås med en mindre indgribende foranstaltning?

Menneskeretten forhindrer ikke at staten eks. etablerer overvågning af personer mistænkt for at have begået eller at ville begå forbrydelser. Det er der allerede hjemmel til, og hvis man ønsker at etablere den form for overvågning, er det nødvendigt at man retfærdiggør indgrebet overfor en dommer.

Når det gælder den danske logning, er der primært tale om overvågning af folk der ikke har begået, og ikke har tænkt sig at begå, en forbrydelse. Det er åbenlyst for enhver at det ville være mindre indgribende, hvis man kun overvågede folk som der er en konkret mistanke til.

[Se _Hvilke rettigheder har vi?_]{custom-style="ref"}

## Hvorfor er det forkert?
Retten til privatliv er en internationalt anerkendt ret, og den overlapper med ytrings- og tankefrihed, retten til forsamlings- og foreningsfrihed, retten til familieliv og retten til personlig frihed.

Et eksempel på behovet for privatliv er seksualitet. Homo- og heteroseksuelle har stadig ikke samme rettigheder i Danmark, så det kan have juridiske konsekvenser at offentliggøre sine seksuelle præferencer. Hvis du besøger en homobar med din telefon i lommen kan staten tilføje dig til listen.

Et andet eksempel er forbudte foreninger. Den danske stat kan forbyde en forening, og logningsdata kan bruges til at lave et register over foreningens medlemmer. Den danske stat har også historik for at anholde alle deltagere i en demonstration. Med logningsdata behøver man ikke gøre det på tidspunktet for demonstrationen. Logningsdata giver staten en praktisk liste over deltagere i enhver demonstration eller forsamling.

## Hvorfor er det ulovligt?
Anden verdenskrig lærte os at folkestemningen kan gøre et land til et modbydeligt sted, og på den baggrund var Danmark med til at skrive og ratificere den Europæiske Menneskerettighedskonvention. Vi opdaterede også grund­ loven så der kan afgives kompetencer til et internationalt organ, som kan sætte grænser for Folketinget. Menneske­ rettighedsdomstolen fik dog aldrig denne magt, men med Lissabon­traktaten har EU indskrevet de tværeuropæiske menneskerettigheder i traktatsgrundlaget. Det betyder at Folketinget ikke længere har mulighed for at fravige de fundamentale rettigheder, selvom 90 mandater ønsker det.

EU’s menneskeret gælder dog kun på områder der er om­ fattet af EU­-samarbejdet. Det inkluderer telefoni, og der­ for har EU-­domstolen kunnet afgøre at logning er ulovligt. Det anerkender den danske stat, men de danske politikere gemmer sig bag en fantasi om at de har en vis indkøringsperiode før de behøver respektere menneskeretten.

Den sidste dom fra EU kom i 2016, og dengang sagde Justitsministeriet at EU-­retten tillader at de bruger op til 12 måneder på at implementere en dom.

Denne sag er relevant, fordi det er den første store sag om EU’s menneskerettigheder i Danmark. Når vi har vundet, vil dommen fremover være befolkningens værn mod politikernes rabiate idéer. Derfor er det vigtigt at støtte sagen. Også selvom man ikke er bange for overvågning.

## Hvad gør Folketinget?
Ingenting. Der bliver stillet spørgsmål og holdt samråd, men intet medlem af Folketinget har afhørt embedsmænd, foreslået at nedsætte en dommerundersøgelse eller foreslået at Rigsretten nedsættes. I grundloven, og i lov om ministres ansvarlighed, står der ellers at dét er næste træk, når en minister så åbenlyst bryder loven.

Skriv til politikerne. Bed dem om at indkalde ministeriets nuværende og tidligere medarbejdere til parlamentarisk afhøring. Bed dem om at kræve alle dokumenter udleveret. Bed dem stille nuværende og tidligere ministre til ansvar.

## Hvornår sker der noget?
Det er svært at sige, for Justitsministeriet og Kammeradvokatens taktik er at trække tiden ud. Hver eneste gang ministeriet har skullet sende noget til domstolene, har de bedt om udsættelse. Pt. har de fået udsættelse til september 2019, men det har vi bedt om at få efterprøvet i Højesteret.

## Hvilke rettigheder har vi?
Den danske grundlov giver dig stort set ingen rettigheder. Den er skrevet for at beskytte borgerne imod kongehuset, men man følte ikke behov for at beskytte imod Folketinget. I dag har kongehuset overført sin magt til Folketinget, og derfor er magten koncentreret på meget få personer. Det står dem frit for at fravige grundlovens rettigheder.

Efter 2. verdenskrig skrev FN Verdenserklæringen om Menneskerettighederne og Europarådet skrev den Europæiske Menneskerettighedskonvention. Den giver teoretisk danskerne de fulde rettigheder, men den har kun lovkraft, og kan i et vist omfang tilsidesættes af Folketinget. EU har dog implementeret den på traktatsniveau, hvilket Folketinget er tvunget til at overholde. Der er dog dén begrænsning at det kun gælder for områder der er reguleret af EU.

De tværeuropæiske menneskerettigheder giver dig bl.a. retten til privatliv, retten til tankefrihed, retten til forenings- og forsamlingsfrihed og retten til ytringsfrihed. Det er disse rettigheder vi bruger til at få standset den ulovlige overvågning.

Når vi har vundet denne sag, er EU's menneskerettigheder endelig etableret i Danmark, og det vil sætte grænser for fremtidige magtfuldkomne politikeres misbrug af befolkningen.

[Se _Hvorfor er I så sure?_]{custom-style="ref"}

## Hvad kan jeg gøre?
Udover at bidrage økonomisk (Mobile Pay 40456, bank 5301 272500) kan du få andre til at bidrage økonomisk. Men du kan også ændre samfundets samtale. Tag emnet op! Derhjemme, i skolen, på arbejdet.

Skriv til medierne, og tag fat på politikerne. Spørg dem hvorfor de tilsidesætter menneskeretten, og hvorfor de ikke gør noget ved det. Spørg dem hvorfor de bliver ved med at indkalde til samråd, når regeringen ikke svarer på spørgsmålene. Spørg dem hvorfor de ikke vil stille hinanden til ansvar. Spørg medierne hvorfor de aldrig bruger tid på danske menneskeretskrænkelser, men har meget travlt med krænkelser i andre lande. Spørg!

## Løgne
Når man ikke har et godt argument, er det oplagt at lyve i stedet. Vi har samlet nogle af de oftest forekommende løgne og deres matchende sandheder.

### Kloge jurister siger at det er lovligt
Nej de gør ikke. Ministeriets forsvar er at de mener at have 12 måneder til at implementere domme der påvirker dansk ret. De 12 måneder begyndte i december 2016.

### Der er domstolskontrol
Nej. Det er indsamlingen af data der er ulovlig, og dér er ingen domstolskontrol. Ift. brugen af de ulovlige data er der mulighed for domstolskontrol, men du er ikke repræsenteret. Sagen er imellem teleselskabet og staten, og selskabet har ingen interesse i at bruge penge på at forsvare dine rettigheder.

Selv hvis du er repræsenteret har du ikke selv valgt din repræsentant, og vedkommende må ikke fortælle dig at der er sket et indgreb. Hvis politiet kræver data, men dine oplysninger beviser at du er uskyldig, er der ingen garanti for at du får det at vide.

[Se _Hvilke rettigheder har vi?_]{custom-style="ref"}

### Det er uundværligt for politiet
Nej. Der er ingen data på denne påstand, og politiet har selv sagt at det ikke kunne bruges imod terror. Se _Hvorfor logges der?_. Det kan dog bruges til at straffe uskyldige. Forestil dig at der bliver malet grafitti på en væg, lørdag kl. 20. Politiet kan få en liste over alle mobiltelefoner i området på gerningstidspunktet, og ud fra den liste kan de vælge dén der er lettest at straffe. Det er herefter op til dig at bevise din uskyld.

### Det stopper pædofili
Nej. Beskyttelse af børn er et populært argument for at undertrykke rettigheder, for hvem vil ikke beskytte børn? Men det er et spørgsmål om proportionalitet og effektivitet. Hvis en person planlægger at kidnappe et barn, er der gode chancer for at vedkommende efterlader telefonen derhjemme. Så en overvågning af næsten 6 mio. person er voldsomt uproportionelt, ift. at kunne opklare færre end en håndfuld sager om året.

[Se bl.a. _"Hvorfor logges der?"_ og _"Det er uundværligt for politiet"_.]{custom-style="ref"}

### Jeg har intet at skjule
Måske har du intet at skjule i dag. Men nuværende danske politiske partier foreslår forskellige rettigheder til forskellige grupper. Måske fjernes retten til dagpenge for folk der har været i udlandet indenfor den seneste måned. Så kan dine data fra i går bruges til at vurdere om du opfylder et krav fra i dag.
