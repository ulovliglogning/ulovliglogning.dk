[![Build Status](https://drone.data.coop/api/badges/ulovliglogning/ulovliglogning.dk/status.svg)](https://drone.data.coop/ulovliglogning/ulovliglogning.dk)    
# Jekyll site for ulovliglogning.dk

## To work on the code, either

### Use docker
You need docker-compose. Simply run
`docker-compose up` to launch the site in a docker container with livereload support. 

### Or install Jekyll locally

#### Install Jekyll and Bundler gems through RubyGems
`gem install jekyll bundler`

#### Install dependencies
`bundle install`

#### Build the site local
`bundle exec jekyll serve`

You can now see the site at [localhost:4000](http://localhost:4000)

## Deploy

If not using Docker, it's **important** that you build the site with `JEKYLL_ENV=production bundle exec jekyll build` before deploying the contents of the `_site` directory. We're doing some debugging stuff when the project is build in the development env.

### Deploy to FTP with Docker
Make sure that you have created a file in `_scripts/` called `config.sh`, (protip: just duplicate the `config-example.sh` file, and set the correct values, you should only need to change username and password.) Then you can build the site ande deploy it by running `docker-compose -f docker-compose-deploy.yml up`

This will execute a Docker container and do the following:
1. Build the site **but** fail if there are any Frontmatter errors
2. Upload the contents of the `_site/` directory to the FTP server
3. That's it…

## Structure
The contents of the index pages is defined in the frontmatter of the files `_pages/index-<language>.md`