FROM jekyll/jekyll:3.7 AS jekyll
ADD . /srv/jekyll
RUN jekyll build --destination /tmp/_site --strict_front_matter

FROM php:7.4.1-apache
#RUN echo "LogLevel alert rewrite:trace6" > /etc/apache2/conf-enabled/rewritelog.conf
RUN a2enmod rewrite
COPY --from=jekyll /tmp/_site /var/www/html